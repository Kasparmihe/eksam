<!DOCTYPE html>
<head>
    <title>Klõps!</title>
    <link href="{{ asset('css/app.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        .navbar-nav{
            margin-top: 15px;
        }
        .nav-item.active {
            margin-left: 50px;
        }
        #carouselExampleIndicators {
            width: 50%;
            height: 50%;
            position: center;
        }
        .d-block->w-100{
            width: 50%;
            height: 50%
        }
    </style>

</head>
<div>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#"><img src="img/logo1.png" style="position: fixed; top: 1%; left: 0.5%"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active" style= "margin-left: 100px">
                    <a class="nav-link" href="#">E-pood</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#">Meist</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#">Kontakt</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#">Tooted</a>
                </li>
                <li class="nav-item" style="position: absolute; right: 10%;">
                    <a class="nav-link" href="#" style="color: red">Logi sisse</a>
                </li>
            </ul>
        </div>
    </nav>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="img/canon.jpg" class="d-block w-100" alt="slide-1">
                </div>
                <div class="carousel-item">
                    <img src="img/garmin.jpg" class="d-block w-100" alt="slide-2">
                </div>
                <div class="carousel-item">
                    <img src="img/sony.jpg" class="d-block w-100" alt="slide-3">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

<script>src="js/app.js/"</script>
</html>

